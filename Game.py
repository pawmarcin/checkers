# Game.py

from Players import *
from Board import *
from Pieces import *

class game:
  
    def __init__(self):
        
        self.p1 = human_player(1)
        self.p2 = ai_player(self, 2)
        self.create_board()
        
        # Set the piece counts to keep track of pieces to determine when the game ends
        self.p1_piece_count = 15
        self.p2_piece_count = 15   
        self.board.show()
        
        while True:
            move = self.p1.get_move_input(self.board)
            if move is not None:
                self.make_move(move)
                if self.check_for_gameover()[0]:
                    break
            else:
                break

            move = self.p2.get_move_input(self.board)
            if move is not None:
                self.make_move(move)
                if self.check_for_gameover()[0]:
                    break
            else:
                break
        
        
    # Storing size of the board
    def create_board(self):
        size = 10
        self.board = board(int(size))
    
    
    # Boolean representing the winner
    def check_for_gameover(self):
        if self.p1_piece_count == 0:
            print("Player 1 has no legal moves. PC wins!")
            return [True, 1]  
        elif self.p2_piece_count == 0:
            print("PC has no legal moves. Player wins!")
            return [True, 2]
        else:
            # Error
            return [False, -1]
      
      
    # Moving the piece, piece-capturing in jumps
    def make_move(self, move):
        
        x1 = move[0]
        y1 = move[1]
        x2 = move[2]
        y2 = move[3]
        
        piece = self.board.get_space(x1, y1)
        
        # Jumping if the move is a jump
        if piece.is_legal_diagonal_jump(x2, y2):
            between_piece = self.board.get_in_between_space(x1, y1, x2, y2)
            self.capture(between_piece)   # Removes the jumped piece
            piece.set_pos(x2, y2)
            self.board.set_space(x2, y2, piece)
            self.board.set_space(x1, y1, self.board.default_char) 
            self.check_for_kinging(piece)   # Checks if the move has placed the piece in a spot to be kinged
            self.check_for_extra_jump(x2, y2)   # Here we check if the piece can jump again
        else:
            piece.set_pos(x2, y2)
            self.board.set_space(x2, y2, piece)
            self.board.set_space(x1, y1, self.board.default_char)
            self.check_for_kinging(piece)   # Checks if the move has placed the piece in a spot to be kinged
   
        self.board.show()
            
    
    # Removes the captured piece from the board
    def capture(self, piece):
        if isinstance(piece, str):
            print(f"Error: Trying to capture a string ({piece}) instead of a piece.")
            return

        print(f"Capturing piece at ({piece.x}, {piece.y})")
        self.board.set_space(piece.x, piece.y, self.board.default_char)
        
        if isinstance(piece, p1_piece):
            self.p1_piece_count -= 1
        elif isinstance(piece, p2_piece):
            self.p2_piece_count -= 1

        print(f"Player 1 pieces remaining: {self.p1_piece_count}")
        print(f"Player 2 pieces remaining: {self.p2_piece_count}")
        
    # Given the position of a piece which has just jumped, 
    def check_for_extra_jump(self, x, y):
        piece = self.board.get_space(x, y)
        
        # p1 pieces 
        if isinstance(piece, p1_piece):
            if (x - 2 >= 0 and y + 2 < self.board.size
                and self.board.get_space(x - 2, y + 2) == self.board.default_char
                and isinstance(self.board.get_in_between_space(x, y, x - 2, y + 2), p2_piece)):
                
                self.confirm_extra_jump(x, y)
                
            if (x + 2 < self.board.size and y + 2 < self.board.size
                and self.board.get_space(x + 2, y + 2) == self.board.default_char
                and isinstance(self.board.get_in_between_space(x, y, x + 2, y + 2), p2_piece)):
    
                self.confirm_extra_jump(x, y)
            
            # p1 king
            if isinstance(piece, p1_king):
                if (x - 2 >= 0 and y - 2 >= 0
                    and self.board.get_space(x - 2, y - 2) == self.board.default_char
                    and isinstance(self.board.get_in_between_space(x, y, x - 2, y - 2), p2_piece)):
                
                    self.confirm_extra_jump(x, y)
                
                if (x + 2 < self.board.size and y - 2 >= 0
                    and self.board.get_space(x + 2, y - 2) == self.board.default_char
                    and isinstance(self.board.get_in_between_space(x, y, x + 2, y - 2), p2_piece)):
                
                    self.confirm_extra_jump(x, y)
    
        # p2 pieces 
        else:
            if (x - 2 >= 0 and y - 2 >= 0
                and self.board.get_space(x - 2, y - 2) == self.board.default_char
                and isinstance(self.board.get_in_between_space(x, y, x - 2, y - 2), p1_piece)):
                
                # If it's an AI player, we always take the jump left side of the board
                if isinstance(self.p2, ai_player):
                    self.board.show()
                    print ('Jumped!')
                    print ('Jumping again...')
                    self.make_move([x, y, x - 2, y - 2])
                else:
                    self.confirm_extra_jump(x, y)
            
            if (x + 2 < self.board.size and y - 2 >= 0
                and self.board.get_space(x + 2, y - 2) == self.board.default_char
                and isinstance(self.board.get_in_between_space(x, y, x + 2, y - 2), p1_piece)):
                
                # If it's an AI player, we always take the jump right side of the board
                if isinstance(self.p2, ai_player):
                    self.board.show()
                    print ('Jumped!')
                    print ('Jumping again...')
                    self.make_move([x, y, x + 2, y - 2])
                else:
                    self.confirm_extra_jump(x, y)
                
            # Tests for opponent kings
            if isinstance(piece, p2_king):
                if (x - 2 >= 0 and y + 2 < self.board.size
                    and self.board.get_space(x - 2, y + 2) == self.board.default_char
                    and isinstance(self.board.get_in_between_space(x, y, x - 2, y + 2), p1_piece)):
                    
                    # If it's an AI player, we always take the jump.
                    if isinstance(self.p2, ai_player):
                        self.board.show()
                        print ('Jumped!')
                        print ('Jumping again...')
                        self.make_move([x, y, x - 2, y + 2])
                    else:
                        self.confirm_extra_jump(x, y)
                
                if (x + 2 < self.board.size and y + 2 < self.board.size
                    and self.board.get_space(x + 2, y + 2) == self.board.default_char
                    and isinstance(self.board.get_in_between_space(x, y, x + 2, y + 2), p1_piece)):
                    
                    # If it's an AI player, we always take the jump
                    if isinstance(self.p2, ai_player):
                        self.board.show()
                        print ('Jumped!')
                        print ('Jumping again...')
                        self.make_move([x, y, x + 2, y + 2])
                    else:
                        self.confirm_extra_jump(x, y)
                        
    
    # Informs the player that another jump is possible and gives them the option of choosing an available extra jump or
    def confirm_extra_jump(self, x1, y1):
        self.board.show()
        destination = input("You can jump again! Choose a space to jump:")
        
        # Parses player input
        x2 = letter_number_parse(destination)[0]
        y2 = letter_number_parse(destination)[1]
        
        piece = self.board.get_space(x1, y1)
        
        if isinstance(piece, p1_piece):
            player = self.p1
        else:
            player = self.p2
        
        # Otherwise, we check if the move is a legal jump and make the move if it is
        if piece.is_legal_diagonal_jump(x2, y2) and player.is_legal_move(self.board, x1, y1, x2, y2)[0]:
            self.make_move([x1, y1, x2, y2])
        # If the move isn't legal, we tell the player and let them make another input
        else:
            print ('Illegal move: You must select either a legal tile to jump to')
            self.confirm_extra_jump(x1, y1)
            
            
    # Checks if the piece is in a position to be kinged and kings it if it is
    def check_for_kinging(self, piece):
        # If a p1 piece reaches the top of the board, we king it
        if isinstance(piece, p1_piece) and piece.y == self.board.size - 1:
            self.king(piece)
        # If an p2 piece reaches the bottom of the board, we king it
        elif piece.y == 0:
            self.king(piece)
            
    
    # Replaces a piece with its kinged variety
    def king(self, piece):
        if isinstance(piece, p1_piece):
            king_piece = p1_king(piece.x, piece.y)
            self.board.set_space(piece.x, piece.y, king_piece)
        else:
            king_piece = p2_king(piece.x, piece.y)
            self.board.set_space(piece.x, piece.y, king_piece)
