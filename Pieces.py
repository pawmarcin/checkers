# Pieces.py

from colorama import Fore, Style
import string

# The parent class for all pieces in the game
class game_piece:
    
    # Stores Board position
    def __init__(self, x=-1, y=-1):
        self.x = x
        self.y = y
    
    # Changes the piece's board position
    def set_pos(self, x, y):
        self.x = x
        self.y = y
        

# Class for all regular player pieces and parent class for player kings
class p1_piece(game_piece):
  
    def __repr__(self):
        return f'{Fore.RED}W{Style.RESET_ALL}'
    
    def is_legal_move(self, x2, y2):
        return self.is_diagonal_step(x2, y2) or self.is_diagonal_step(x2, y2)
    
    def is_legal_diagonal_step(self, x2, y2):
        return (x2 == self.x + 1 or x2 == self.x - 1) and (y2 == self.y + 1)
    
    def is_legal_diagonal_jump(self, x2, y2):
        return (x2 == self.x + 2 or x2 == self.x - 2) and (y2 == self.y + 2)
    

# Class for player kings
class p1_king(p1_piece):
  
    def __repr__(self):
        return f'{Fore.RED}V{Style.RESET_ALL}'
    
    def is_legal_diagonal_step(self, x2, y2):
        return (x2 == self.x + 1 or x2 == self.x - 1) and (y2 == self.y + 1 or y2 == self.y - 1)
    
    def is_legal_diagonal_jump(self, x2, y2):
        return (x2 == self.x + 2 or x2 == self.x - 2) and (y2 == self.y + 2 or y2 == self.y - 2)
    

# Class for all regular PC pieces and parent class for PC kings
class p2_piece(game_piece):
  
    def __repr__(self):
        return f'{Fore.WHITE}●{Style.RESET_ALL}'
    
    def is_legal_move(self, x2, y2):
        return self.is_diagonal_step(x2, y2) or self.is_diagonal_step(x2, y2)
    
    def is_legal_diagonal_step(self, x2, y2):
        return (x2 == self.x + 1 or x2 == self.x - 1) and (y2 == self.y - 1)
    
    def is_legal_diagonal_jump(self, x2, y2):
        return (x2 == self.x + 2 or x2 == self.x - 2) and (y2 == self.y - 2)
    


# Class for PC kings
class p2_king(p2_piece):
  
    def __repr__(self):
        return f'{Fore.YELLOW}●{Style.RESET_ALL}'
    
    def is_legal_diagonal_step(self, x2, y2):
        return (x2 == self.x + 1 or x2 == self.x - 1) and (y2 == self.y + 1 or y2 == self.y - 1)
    
    def is_legal_diagonal_jump(self, x2, y2):
        return (x2 == self.x + 2 or x2 == self.x - 2) and (y2 == self.y + 2 or y2 == self.y - 2)
  
  
# Takes a string of letters and numbers and converts them into coordinates
def letter_number_parse(s):
    if len(s) < 2:
        raise ValueError("Input too short")
    
    # First, we split the string up into substrings representing the row and column of the input
    row = s[0]
    col = s[1:]
    
    # Now, we convert these strings into their corresponding coordinate values
    row_coord = string.ascii_lowercase.index(row.lower())
    col_coord = int(col) - 1
    # Return them in a list
    return [row_coord, col_coord]


# Used to test whether a user input is valid
def is_numbers(s):
    for c in s:
        if not c in string.digits:
            return False
    return True
