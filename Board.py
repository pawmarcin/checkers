# Board.py
from Pieces import *

class board:
  
    # Given a size and default character, initializes a board of that size using that character to represent empty board spaces.
    def __init__(self, size=10, default_char=' '):  # Board size is customizable!
        self.rep = []
        self.size = size                                       
        self.default_char = default_char
    
        # Creates a |size * size__ board
        for i in range(size):
            row = []
            for j in range(size):
                # Change position every second place
                if (i + j) % 2 == 0:
                    row.append(default_char)
                else:
                    # Leave empty space
                    row.append("■")  
            self.rep.append(row)
        
        # Populates the board with pieces
        for i in range(size//2):
            
            # Top 3 rows: can be adjust to size of the board
            self.rep[size - 1][1 + 2*i] = p2_piece(1 + 2*i, size - 1)   # Adding one to the second index offsets the pieces so that
            self.rep[size - 2][2*i] = p2_piece(2*i, size - 2)           # we get a nice checkerboard-style distribution.
            self.rep[size - 3][1 + 2*i] = p2_piece(1 + 2*i, size - 3)
            
            # Bottom 3 rows: can be adjust to size of the board
            self.rep[2][2*i] = p1_piece(2*i, 2)
            self.rep[1][1 + 2*i] = p1_piece(1 + 2*i, 1)
            self.rep[0][2*i] = p1_piece(2*i, 0)
  
    
    # Given an x and y coordinate, returns the item held at that board space.
    def get_space(self, x, y):
    
        answer = self.rep[y][x]
        return answer
    
    
    # Given an x and y coordinate, fills that board space with a new item.
    def set_space(self, x, y, new_thing):
    
        self.rep[y][x] = new_thing
    
    
    # Returns the space in between two given tiles.
    def get_in_between_space(self, x1, y1, x2, y2):
        assert (abs(x1 - x2) == abs(y1 - y2) == 2)   # Ensures that the spaces do indeed have a space between them
    
        return self.get_space(int((x1 + x2) / 2), int((y1 + y2) / 2))
    
    
    # Returns the next space behind two spaces in a diagonal line. 
    def get_behind_space(self, x1, y1, x2, y2):
        assert(abs(x1 - x2) == abs(y1 - y2) == 1)   
        return self.get_space(2 * x2 - x1, 2 * y2 - y1)
    
    
    # Clears space on the screen and displays the board
    def show(self):
        print(self)
  
  
    # Represents the board 
    def __repr__(self):
        result = ""
    
        # Converts the list of lists into a string representing the board
        for r in range(self.size):
            row = ""
            for i in self.rep[r]:
                row = row + str(i) + " "
            result = "\n" + row + ' ' + str(r + 1) + result   
        guide_row = ''
        for i in range(self.size):
            guide_row = guide_row + string.ascii_uppercase[i] + ' '
      
        result = result + '\n' + guide_row
        return result + '\n'
