# Players.py

from Pieces import *
import string
import random

# Class for human and PC player
class player:
    
    # Takes in and stores an int team representing the team the player is on
    def __init__(self, team):
        self.team = team
        
    # Checks if a move is legal
    def is_legal_move(self, board, x1, y1, x2, y2):
        
        # Orients the tests according to which player the move is for
        if self.team == 1:
            friend = p1_piece
            foe = p2_piece
        elif self.team == 2:
            friend = p2_piece
            foe = p1_piece
        
        # Checks that the move's origin and destination fall within the ranges of the board
        if not (0 <= x1 <= board.size - 1
            and 0 <= y1 <= board.size - 1
            and 0 <= x2 <= board.size - 1
            and 0 <= y2 <= board.size - 1):
            return [False, 'Range of the board!']
        
        piece = board.get_space(x1, y1)
        
        # Checks that the piece being moved is a friendly piece
        if not isinstance(piece, friend):
            return [False, 'You must choose a tile holding one of your own pieces!']
        
        # Checks that the destination space is empty
        elif isinstance(board.get_space(x2, y2), game_piece):
            return [False, 'Destination space must be empty!']
      
        # Checks that the move is a diagonal step or jump
        elif not (piece.is_legal_diagonal_step(x2, y2) or piece.is_legal_diagonal_jump(x2, y2)):
            return [False, 'Move must be a legal diagonal step or jump!']
        
        # Checks that jumps are made over enemy pieces
        elif piece.is_legal_diagonal_jump(x2, y2) and not isinstance(board.get_in_between_space(x1, y1, x2, y2), foe):
            return [False, 'You can only jump over opponent pieces!']
      
        # Returns True if none of these rules are violated
        else:
            return [True, '']
        

class human_player(player):
    
    def get_move_input(self, board):
        
        # Interfaces with human player
        print('PLAYER TURN:')
        
        # Ensures the player's inputs are in the acceptable form
        def get_user_input(prompt):
            acceptable_input = False
            while not acceptable_input:
                user_input = input(prompt)
                if len(user_input) >= 2 and user_input[0] in string.ascii_letters and user_input[1:].isdigit():
                    acceptable_input = True
                else:
                    print('Wrong input: Your input must be in the form of a letter followed by a number!')
            return user_input

        origin = get_user_input('Choose a piece to move: ')
        destination = get_user_input('Choose a tile to move it to: ')
                    
        # Parses and stores human input into x-y-coordinates
        x1 = letter_number_parse(origin)[0]
        y1 = letter_number_parse(origin)[1]
        x2 = letter_number_parse(destination)[0]
        y2 = letter_number_parse(destination)[1]
        
        # Checks if the move is legal
        legality = self.is_legal_move(board, x1, y1, x2, y2)
        
        # If the move is illegal, we tell the player why it failed and give them the chance to take another turn.
        if not legality[0]:
            print(('Illegal move: ') + legality[1])
            board.show()
            return self.get_move_input(board)
        else:
            return [x1, y1, x2, y2]

# PC player
class ai_player(player):
    def __init__(self, team, game):
        super().__init__(team)
        self.moves_since_jump = 0
        self.game = game
        
    def get_move_input(self, board):
        
        print('PC TURN:')      
        move = self.find_legal_move(board)
        
        if move is not None:
            x1, y1, x2, y2 = move

            if board.get_space(x1, y1).is_legal_diagonal_jump(x2, y2):
                self.moves_since_jump = 0
            else:
                self.moves_since_jump += 1 

            return [x1, y1, x2, y2]
        else:
            
            print("No legal moves available. Game over.")
            
    
    def find_legal_move(self, board):
        # Check for potential capture 
            
            capture_moves = []
            
            for x1 in range(board.size):
                for y1 in range(board.size):
                    piece = board.get_space(x1, y1)

                    if isinstance(piece, p2_piece):
                        # Check jumps
                        if x1 - 2 >= 0 and y1 - 2 >= 0 and board.get_space(x1 - 2, y1 - 2) == board.default_char:
                            if isinstance(board.get_in_between_space(x1, y1, x1 - 2, y1 - 2), p1_piece):
                                capture_moves.append([x1, y1, x1 - 2, y1 - 2])
                        if x1 + 2 < board.size and y1 - 2 >= 0 and board.get_space(x1 + 2, y1 - 2) == board.default_char:
                            if isinstance(board.get_in_between_space(x1, y1, x1 + 2, y1 - 2), p1_piece):
                                capture_moves.append([x1, y1, x1 + 2, y1 - 2])

                    if isinstance(piece, p2_king):
                        # Check jumps ahead
                        if x1 - 2 >= 0 and y1 - 2 >= 0 and board.get_space(x1 - 2, y1 - 2) == board.default_char:
                            if isinstance(board.get_in_between_space(x1, y1, x1 - 2, y1 - 2), p1_piece):
                                capture_moves.append([x1, y1, x1 - 2, y1 - 2])
                        if x1 + 2 < board.size and y1 - 2 >= 0 and board.get_space(x1 + 2, y1 - 2) == board.default_char:
                            if isinstance(board.get_in_between_space(x1, y1, x1 + 2, y1 - 2), p1_piece):
                                capture_moves.append([x1, y1, x1 + 2, y1 - 2])
                        # Check jumps backwards
                        if x1 - 2 >= 0 and y1 + 2 < board.size and board.get_space(x1 - 2, y1 + 2) == board.default_char:
                            if isinstance(board.get_in_between_space(x1, y1, x1 - 2, y1 + 2), p1_piece):
                                capture_moves.append([x1, y1, x1 - 2, y1 + 2])
                        if x1 + 2 < board.size and y1 + 2 < board.size and board.get_space(x1 + 2, y1 + 2) == board.default_char:
                            if isinstance(board.get_in_between_space(x1, y1, x1 + 2, y1 + 2), p1_piece):
                                capture_moves.append([x1, y1, x1 + 2, y1 + 2])
                                
            legal_moves = []

            for x1 in range(board.size):
                for y1 in range(board.size):
                    piece = board.get_space(x1, y1)

                    if isinstance(piece, p2_piece):
                        # Check legal moves
                        if x1 - 1 >= 0 and y1 - 1 >= 0 and board.get_space(x1 - 1, y1 - 1) == board.default_char:
                            legal_moves.append([x1, y1, x1 - 1, y1 - 1])
                        if x1 + 1 < board.size and y1 - 1 >= 0 and board.get_space(x1 + 1, y1 - 1) == board.default_char:
                                legal_moves.append([x1, y1, x1 + 1, y1 - 1])

                    if isinstance(piece, p2_king):
                            # Check legal moves
                            if x1 - 1 >= 0 and y1 - 1 >= 0 and board.get_space(x1 - 1, y1 - 1) == board.default_char:
                                legal_moves.append([x1, y1, x1 - 1, y1 - 1])
                            if x1 + 1 < board.size and y1 - 1 >= 0 and board.get_space(x1 + 1, y1 - 1) == board.default_char:
                                legal_moves.append([x1, y1, x1 + 1, y1 - 1])   
                            if x1 - 1 >= 0 and y1 - 1 >= 0 and board.get_space(x1 - 1, y1 + 1) == board.default_char:
                                legal_moves.append([x1, y1, x1 - 1, y1 + 1])
                            if x1 + 1 < board.size and y1 - 1 >= 0 and board.get_space(x1 + 1, y1 + 1) == board.default_char:
                                legal_moves.append([x1, y1, x1 + 1, y1 + 1]) 
                                
            # Check for capture
            if len(capture_moves) > 0:
                return random.choice(capture_moves)  
                      
            # Else random legal move
            elif len(legal_moves) > 0 and len(capture_moves) == 0:    
                return random.choice(legal_moves) 
            
            # If no more legal moves and no more captures = Game Over 
            elif len(capture_moves) == 0 and len(legal_moves) == 0:
                return None
